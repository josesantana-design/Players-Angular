var myApp = angular.module('myApp', ['ngRoute', 'playerControllers']);

myApp.config(['$routeProvider', function($routeProvide){
	
	$routeProvide.when('/lista', {
		templateUrl: 'parciales/lista.html',
		controller: 'ListaController'
	})
	.when('/detalles/:itemID', {
		templateUrl: 'parciales/detalles.html',
		controller: 'DetallesController'
	})
	.otherwise({
		redirectTo: '/lista'
	})
}]);