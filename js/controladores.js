// Controlador
var playerControllers= angular.module('playerControllers', []);

playerControllers.controller('ListaController', ['$scope', '$http', function($scope, $http){
	// Consumiendo archivo JSON
	$http.get('js/data.json').success(function(data){
		$scope.players = data;
	});

}]);

playerControllers.controller('DetallesController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){
	// Consumiendo archivo JSON
	$http.get('js/data.json').success(function(data){
		$scope.players = data;
		$scope.whichItem = $routeParams.itemID;
	});

}]);
